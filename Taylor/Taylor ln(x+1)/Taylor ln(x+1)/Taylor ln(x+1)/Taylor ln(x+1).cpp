﻿#include <iostream>
#include <iomanip>
#include <cmath>

// Taylor ln( x + 1 )
double taylorLn(double x, double epsilon = 1e-5) {
	double p, res, buf;
	int i;
	p = res = x;
	i = 2;
	while (fabs(p) >= epsilon){
		p *= -x;
		res += p / i;
		i++;
	}
	return res;
}

// Assembly analogue
double asmTaylorLn(double x, double epsilon = 1e-5) {
	int n = 2;	// Calculations are starting from second element ( where denominator is 2)
				// First element is already given - x
	__asm {
		finit
		fld		x
		fld		st(0)					; Register of x degrees

		FOR :
		; FI - First Iteration, SI - second and so on...

			fld		x					; FI: x->st(0), x->st(1), x->st(2)
			fmulp   st(1), st(0)		; FI: x*x->st(0), x->st(1)
			fchs						; FI: -x * x->st(0), x->st(1)

			; Calculating the denominator
			fild	n

			; Calculating the Taylor series element
			fdivr   st(0), st(1)		; FI: -(x*x) / 2->st(0), x*x->st(1), x->st(2)

			; Adding a Taylor Series Element
			fadd    st(2), st(0)		; FI: -(x*x) / 2->st(0), x*x->st(1), x - (x*x) / 2->st(2)
			fstp    st(0)
			fld		st(0)				; Loading copy to abs() it and compare with epsilon (Since we dont't want to loose number sign)

			; Statement
			fabs
			fld     epsilon
			fcomi   st(0), st(1)
			fstp    st(0)
			fstp    st(0)
			inc     n
			jbe     FOR					; Repeat iteration if statment is false
			fstp    st(0)				; Clear stack to result element
	}
}

// UI
int main(){
	int numargs = 6;
	double args[] = { -0.9, -0.5, -0.5, 0.0, 0.5, 0.9};		// Array of values to test function (Radius of convergence for taylor series is (-1, 1))
	
	for (int i = 0; i < numargs; i++){
		std::cout << std::setprecision(12) << "log(" << args[i] << " + 1)" << std::endl <<	// setprecision() to control amount of numbers after decimal point
			"Original: " << asmTaylorLn(args[i]) << std::endl <<
			"Ideal:    " << log(args[i]+1) << std::endl <<									// Function from library 
			"Error:    " << 1 - asmTaylorLn(args[i]) / log(args[i] + 1) << std::endl << std::endl;
	}
	return 0;
}