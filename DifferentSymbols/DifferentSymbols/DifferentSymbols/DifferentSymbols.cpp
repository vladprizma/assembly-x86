﻿#include <iostream>
#include <conio.h> 

// Symbols in first string that second string doesn't have
void differentSymbols(char* s1, char* s2) {
	int res[256];
	for (int z = 0; z < 256; z++)
		res[z] = 0;

	// Заполнение по первой строке
	for (int i = 0; i < strlen(s1); i++)
		res[s1[i]] = 1;

	// Очистка символов, присутствующих во второй строке
	for (int j = 0; j < strlen(s2); j++)
		res[s2[j]] = 0;

	// Вывод результатов
	for (int k = 0; k < 256; k++){
		if (res[k] == 1) {
			std::cout << (char)k << ' ';
		}
	}
}

// Assembly analogue (т.к тут ввод/вывод то это не совсем функция, выполняющая поставленную цель, поэтому код в main)
void asmDifferentSymbols(char* s1, char* s2) {
	char input1[] = "input first string:\n";
	char input2[] = "input second string:\n";
	char result[] = "%c ";

	int res[256];
	int resSize = 256; // Кол-во ascii символов

	__asm {
		; Вывод
		lea     eax, input1
		push    eax
		call    printf
		add     esp, 4

		; Ввод данных первой строки
		lea     edx, s1
		push    256
		push    edx
		call    gets_s
		add     esp, 8

		; Подготовка данных к алгоритму
		lea     eax, s1
		push    eax
		call    strlen
		add     esp, 4

		; Обнуление массива
		lea		ebx, res
		mov		esi, 0
		; ebx - res
		; esi - z
		FOR_Z :
			mov		[ebx][esi], 0		; ebx[esi] = 0
			inc		esi; i++
			cmp     esi, 1024			; i < 256*4 ? (Для нормального вывода надо домножать на 4 и в 195 строке(на 2 выше) убрать умножение на 4)
			jl      FOR_Z; if (i < 0)

			; Заполнение единицами, символами из первой строки
			lea     edx, s1
			mov		esi, 0
			mov		ecx, 0
			; ebx - res
			; edx - s1
			; eax - len1
			; esi - i
			FOR_I :
		mov		ecx, 0
			mov		cl, [edx][esi]; т.к.однобайтовый регистр хранит имеет столько же места сколько и ascii таблица, то в al не будет лишнего
			mov		edi, 0
			add		edi, ecx; единственный способ сложить al в edi
			mov		cl, 1
			mov[ebx][edi * 4], cl; По int значению char ставим 1. edi * 4 - т.к. 4 - кол - во байт типа int, для сдвига
			inc		esi
			cmp		esi, eax; i < check.len
			jl	FOR_I

			; Вывод
			lea     eax, input2
			push    eax
			call    printf
			add     esp, 4

			; Ввод данных второй строки
			lea     edx, s2
			push    256
			push    edx
			call    gets_s
			add     esp, 8

			; Подготовка данных к алгоритму
			lea     ecx, s2
			push    ecx
			call    strlen
			add     esp, 4


			; Заполнение нулями, символами из второй строки
			lea     edx, s2
			mov		esi, 0
			; ebx - res
			; edx - s2
			; eax - len2
			; esi - j
			FOR_J :
		mov		ecx, 0
			mov		cl, [edx][esi]; т.к.однобайтовый регистр хранит имеет столько же места сколько и ascii таблица, то в al не будет лишнего
			mov		edi, 0
			add		edi, ecx; единственный способ сложить al в edi
			mov		cl, 0
			mov[ebx][edi * 4], cl; По int значению char ставим 1. edi * 4 - т.к. 4 - кол - во байт типа int, для сдвига
			inc		esi
			cmp		esi, eax; j < check.len
			jl	FOR_J


			; Вывод "массива char"
			mov		esi, 0
			; esi - k
			FOR_K :
		mov		dl, [ebx][esi * 4]
			cmp		dl, 1
			jne		NOT_PRINT
			lea		eax, result
			push	esi; Если res[i] == 1; То это i - ый символ из таблицы ascii, который нам нужен
			push	eax
			call	printf
			add		esp, 8
			NOT_PRINT:
		inc esi
			cmp		esi, 256; k < check.len
			jl	FOR_K

	}
}

// UI
int main() {
	char input1[] = "input first string:\n";
	char input2[] = "input second string:\n";
	char result[] = "%c ";
	char s1[256];
	char s2[256];

	int res[256];
	int resSize = 256; // Кол-во ascii символов

	char continu = '1'; 
	/*
	//ascii таблица
	char inputTest[256];
	for (int i = 0; i < 256; i++) {
		inputTest[i] = (char)i;
	}
	for (int i = 0; i < 256; i++) {
		std::cout << "index: " << i << " int " << (int) inputTest[i] << " char: " << inputTest[i] << std::endl;
	}
	*/
	//char test[] = "%d ";

	while (continu != '0') {
		__asm {
			; Вывод
			lea     eax, input1
			push    eax
			call    printf
			add     esp, 4

			; Ввод данных первой строки
			lea     edx, s1
			push    256
			push    edx
			call    gets_s
			add     esp, 8

			; Подготовка данных к алгоритму
			lea     eax, s1
			push    eax
			call    strlen
			add     esp, 4

			; Обнуление массива
			lea		ebx, res
			mov		esi, 0
			; ebx - res
			; esi - z
		FOR_Z:
				mov		[ebx][esi*4], 0			; ebx[esi] = 0
				inc		esi						; i++
				cmp     esi, 256				; i < 256*4 ? (Для нормального вывода надо домножать на 4 и в 195 строке (на 2 выше) убрать умножение на 4)
				jl      FOR_Z					; if( i < 0 )

			; Заполнение единицами, символами из первой строки
				lea     edx, s1
				mov		esi, 0
				mov		ecx, 0
				; ebx - res
				; edx - s1
				; eax - len1
				; esi - i
					FOR_I :
				mov		ecx, 0
				mov		cl, [edx][esi]		; т.к. однобайтовый регистр хранит имеет столько же места сколько и ascii таблица, то в al не будет лишнего
				mov		edi, 0
				add		edi, ecx			; единственный способ сложить al в edi
 				mov		cl, 1
				mov		[ebx][edi * 4], cl	; По int значению char ставим 1. edi*4 - т.к. 4 - кол-во байт типа int, для сдвига
				inc		esi
				cmp		esi, eax; i < check.len
				jl	FOR_I

			; Вывод 
				lea     eax, input2		
				push    eax				
				call    printf			
				add     esp, 4			

			; Ввод данных второй строки
				lea     edx, s2				
				push    256					
				push    edx					 
				call    gets_s
				add     esp, 8				

			; Подготовка данных к алгоритму
				lea     ecx, s2				 
				push    ecx					 
				call    strlen				 
				add     esp, 4				


			; Заполнение нулями, символами из второй строки
				lea     edx, s2
				mov		esi, 0
				; ebx - res
				; edx - s2
				; eax - len2
				; esi - j
		FOR_J :
				mov		ecx, 0
				mov		cl, [edx][esi]	; т.к. однобайтовый регистр хранит имеет столько же места сколько и ascii таблица, то в al не будет лишнего
				mov		edi, 0
				add		edi, ecx		; единственный способ сложить al в edi
				mov		cl, 0
				mov[ebx][edi * 4], cl	; По int значению char ставим 1. edi*4 - т.к. 4 - кол-во байт типа int, для сдвига
				inc		esi
				cmp		esi, eax; j < check.len
				jl	FOR_J


			; Вывод "массива char"
				mov		esi, 0
				; esi - k
		FOR_K :
				mov		dl, [ebx][esi * 4]
				cmp		dl, 1
				jne		NOT_PRINT
				lea		eax, result
				push	esi		; Если res[i] == 1; То это i-ый символ из таблицы ascii, который нам нужен
				push	eax
				call	printf
				add		esp, 8
		NOT_PRINT:
				inc esi
				cmp		esi, 256; k < check.len
				jl	FOR_K

		}
		//asmDifferentSymbols(s1, s2); тот же эффект
		std::cout << std::endl;
		std::cout << "Press any symbol to continue or 0 to exit the program" << std::endl;
		continu = _getch();
		s1[0] = '\0';
		s2[0] = '\0';
	}
}

/*
					//Вывод
					mov		esi, 0
					; esi - k
				FOR_TEST2 :
					movsx		edx, [ebx][esi * 4]
					lea		eax, test
					push	edx
					push	eax
					call	printf
					add		esp, 8
					inc		esi
					cmp		esi, 256; k < check.len
					jl	FOR_TEST2
*/